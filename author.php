<?php
  //No direct access
  if ($page->url == $_SERVER["REQUEST_URI"]) 
    throw new Wire404Exception();
?>
<div class='authorboxwrap'>
<!--  <div class='authorboxfull'>-->
    <div class='avatar-container'>
      <img alt='<?php echo $page->author_name; ?>' class='author_avatar' height='96' src="<?php echo $page->author_photo->url; ?>" width='96'/>
      <h4>
        <a href='#' rel='author'><?php echo $page->author_name; ?></a>
      </h4>
    </div>
    <div class='author_description_container'>
      <p><?php echo $page->author_description; ?></p>
      <div class='authorsocial'>
        <a class='' href='http://facebook.com/' target='_blank'><i class='fa fa-facebook'></i></a>
        <a class='' href='http://twitter.com/' target='_blank'><i class='fa fa-twitter'></i></a>
        <a class='' href='http://dribbble.com/' target='_blank'><i class='fa fa-dribbble'></i></a>
        <a class='' href='http://instagram.com/' target='_blank'><i class='fa fa-instagram'></i></a>
        <div class='clr'></div>
      </div>
    </div>
<!--  </div>-->
</div>