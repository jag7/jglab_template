<!-- header -->
<?php
echo $pages->get('/templates/header/')->render(null,
array('title' => $page->title,
'stylesheets' => array(
  $config->urls->templates . 'css/comments.css',
)));
?>
<?php
// look for a GET variable named 'q' and sanitize it
$q = $sanitizer->text($input->get->q);

// did $q have anything in it?
if($q) {
  // Send our sanitized query 'q' variable to the whitelist where it will be
  // picked up and echoed in the search box by _main.php file. Now we could just use
  // another variable initialized in _init.php for this, but it's a best practice
  // to use this whitelist since it can be read by other modules. That becomes
  // valuable when it comes to things like pagination.
  $input->whitelist('q', $q);

  // Sanitize for placement within a selector string. This is important for any
  // values that you plan to bundle in a selector string like we are doing here.
  $q = $sanitizer->selectorValue($q);

  // Search the title and body fields for our query text.
  // Limit the results to 50 pages.
  $selector = "title|contenido_post|labels_post~=$q, limit=50";

  // If user has access to admin pages, lets exclude them from the search results.
  // Note that 2 is the ID of the admin page, so this excludes all results that have
  // that page as one of the parents/ancestors. This isn't necessary if the user
  // doesn't have access to view admin pages. So it's not technically necessary to
  // have this here, but we thought it might be a good way to introduce has_parent.
  if($user->isLoggedin()) $selector .= ", has_parent!=2";

  // Find pages that match the selector
  $matches = $pages->get("/blog/")->children->find($selector);

  // did we find any matches?
  if($matches->count):
    // yes we did
    ?>

    <div class="main-container">
      <section id="main" class="pure-u-1 pure-u-md-3-4">
        <div class="status-msg-wrap">
          <div class="status-msg-body">
            Showing posts sorted by relevance for query <b><?php echo $q; ?></b>. <a href="<?php echo $page->httpUrl . "?q=" . $q; ?>&sort=by-date">Sort by date</a> <a href="http://jglab.me">Show all posts</a>
          </div>
          <div class="status-msg-border">
            <div class="status-msg-bg">
              <div class="status-msg-hidden">Showing posts sorted by relevance for query <b><?php echo $q; ?></b>. <a href="<?php echo $page->httpUrl . "?q=" . $q; ?>&sort=by-date">Sort by date</a> <a href="http://jglab.me">Show all posts</a></div>
            </div>
          </div>
        </div>
        <div style="clear: both;"></div>
          <?php foreach($matches as $post): ?>
            <article>
              <?php echo wireRenderFile('partials/article.php',
                    array('post' => $post,
                          'author' => false,
                          'comments' => false,
                          'excerpt' => true,
                          'pagination' => false,
                          'bottom_labels' => false));
              ?>
            </article>
          <?php endforeach; ?>
      </section>
      <!-- /main-wrapper -->
      <!-- sidebar-wrapper -->
      <aside class='sidebar-wrapper pure-u-1 pure-u-md-1-4'>
          <?php echo wireRenderFile('sidebar.php', array()); ?>
      </aside>
      <!-- /sidebar-wrapper -->
    </div>
  <?php endif; ?>
  <?php
} else {
  // no search terms provided
  $content = "<h2>Please enter a search term in the search box (upper right corner)</h2>";
  echo $content;
}
?>
</div>
</div>
<!-- footer -->
<?php
echo $pages->get('/templates/footer/')->render(null,
array('scripts' => array(
  'jquery',
)));
?>
</body>
</html>
