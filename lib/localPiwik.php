<?php
	use Piwik\API\Request;
	use Piwik\FrontController;

	define('PIWIK_INCLUDE_PATH', $_SERVER['DOCUMENT_ROOT'] . "/piwik/");
	define('PIWIK_USER_PATH', $_SERVER['DOCUMENT_ROOT'] . "/piwik/");
	define('PIWIK_ENABLE_DISPATCH', false);
	define('PIWIK_ENABLE_ERROR_HANDLER', true);
	define('PIWIK_ENABLE_SESSION_START', false);

	// if you prefer not to include 'index.php', you must also define here PIWIK_DOCUMENT_ROOT
	// and include "libs/upgradephp/upgrade.php" and "core/Loader.php"
	require_once PIWIK_INCLUDE_PATH . "/index.php";
	require_once PIWIK_INCLUDE_PATH . "/core/API/Request.php";

	$environment = new \Piwik\Application\Environment(null);
	$environment->init();

	FrontController::getInstance()->init();

	class localPiwik{

		private function getPageUrl($url){
			// This inits the API Request with the specified parameters
			$request = new Request("
						module=API
						&method=Actions.getPageUrl
						&idSite=1
						&date=today
						&pageUrl=$url
						&period=year
						&format=original
						&token_auth=01509cd27eef7b25ede61f48c83dcce7
			");
			// Calls the API and fetch data back
			$result = $request->process();
			return $result;
		}

		public function getUniqueVisits($url){
			$result = $this->getPageUrl($url);
			if($result->getRowFromLabel('/index'))
				return $result->getRowFromLabel('/index')->getColumn('sum_daily_nb_uniq_visitors');
			else return 0;
		}
	}

	// //usage
 // 	$piwik = new localPiwik();
 // 	$visits = $piwik->getUniqueVisits("http://jglab.me/blog/facebook-sharer-and-ipv6-issue/");
	// //echo "http://jglab.me/blog/facebook-sharer-and-ipv6-issue/ ha recibido " . $visits . " visita(s)";
?>
