<?php
  function excerpt($string, $max_chars, $end_string = '...'){
    $content = substr($string, 0, $max_chars);
    $pos = strrpos($content, " ");
    if ($pos > 0) {
      $content = substr($content, 0, $pos);
    }
    return $content . '...';
  }

  //posts ordenados por visitas, de mayor a menor
  function top_posts($posts_PW){
    include_once('localPiwik.php');
    $piwik = new localPiwik();
    $posts = array();
    foreach ($posts_PW as $post){
      $visits = $piwik->getUniqueVisits($post->httpUrl);
      if(!is_null($visits)){
        $posts[$post->url] = $visits;
      }
      else{
        $posts[$post->url] = 0;
      }
    }
    asort($posts);
    $posts = array_reverse($posts);
    return array_slice($posts, 0 , 5);
  }

  // top labels
  function top_labels($posts_PW){
    $labelsArr = array();
    foreach ($posts_PW as $post){
      $labels = explode(",", $post->labels_post);
      foreach ($labels as $label){
        $label = str_replace(" ", "", $label);
        if(!isset($labelsArr[$label])){
          $labelsArr[$label] = 0;
        }
        if($labelsArr[$label] > 0){
          $labelsArr[$label]++;
        }
        else{
          $labelsArr[$label] = 1;
        }
      }
    }
    asort($labelsArr);
    $labelsArr = array_reverse($labelsArr);
    return array_slice($labelsArr, 0, 10);
  }

?>
