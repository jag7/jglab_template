<!-- header -->
<?php
echo $pages->get('/templates/header/')->render(null,
array('title' => $page->title,
'stylesheets' => array(
  /*$config->urls->templates . */'css/comments.css',
  /*$config->urls->templates . */'css/loader.css',
)));
?>

  <div class="cover">
    <div class="loading">
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
    </div>
  </div>
  <div class="main-container">
    <section id="main" class="pure-u-1 pure-u-md-3-4">
      <article>
        <?php echo wireRenderFile('partials/article.php',
              array('post' => $page,
                    'author' => true,
                    'comments' => true,
                    'excerpt' => false,
                    'pagination' => false,
                    'bottom_labels' => true));
        ?>
      </article>
    </section>
      <!-- sidebar-wrapper -->
      <aside class='sidebar-wrapper pure-u-1 pure-u-md-1-4'>
          <?php echo wireRenderFile('sidebar.php', array()); ?>
      </aside>
      <!-- /sidebar-wrapper -->
</div>
<!-- footer -->
<?php
echo $pages->get('/templates/footer/')->render(null,
array('scripts' => array(
  'jquery',
  'recaptcha',
  /*$config->urls->templates .*/ 'js/comments.js',
  /*$config->urls->templates .*/ 'js/signature_gen.js'
)));
?>

<!-- form -->
<!-- <form>
  <input type="text" id="username">
  <input type ="button" id="get_signature" value="get it!" disabled>
  <div style="margin-top: 10px;" class="g-recaptcha" data-callback="enableBtn" data-sitekey="6LeJohATAAAAANfx9p94ikgOppoCQYQxk5YHuq-u"></div>
</form> -->
</html>
