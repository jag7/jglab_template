<?php
  //No direct access
  if ($page->url == $_SERVER["REQUEST_URI"])
    // throw new Wire404Exception();
?>
<?php
  $labelsArr = top_labels($pages->get("/blog/")->children);
?>
<div class='widget label-list'>
  <div class="widget-title"><h2><?php echo ('Tag Cloud') ?></h2></div>
  <div class='widget-content'>
    <ul>
      <?php foreach($labelsArr as $label => $count){ ?>
        <li>
          <a dir='ltr' href=<?php echo $pages->get("/search/")->httpUrl . "?q=$label" ?>><?php echo $label; ?>
            <span class='detail-span'><?php echo " [$count]"; ?></span>
          </a>
        </li>
      <?php } ?>
    </ul>
  </div>
</div>
