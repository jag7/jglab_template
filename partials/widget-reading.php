<?php
  //No direct access
  if ($page->url == $_SERVER["REQUEST_URI"])
    // throw new Wire404Exception();
?>
<div class='widget currently-reading'>
  <div class="widget-title"><h2><?php echo ('Currently Reading') ?></h2></div>
  <div class='widget-content'>
    <div class="loading-basic">
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
    </div>
  </div>
</div>
