<div class='comments' id='comments'>
  <?php
  echo $post->comments->render();
  echo $post->comments->renderForm(array('requireSecurityField' => 'security_field'));
  ?>
  <p class='comment-footer'>
    <div class='comment-form'>
      <a name='comment-form'></a>
      <p></p>
    </div>
  </p>
  <div id='backlinks-container'>
    <div id='Blog1_backlinks-container'></div>
  </div>
</div>
