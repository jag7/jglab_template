<?php
// available vars:
//   $post <- page object
?>
<div class="post-container">
  <?php if(isset($image) && $image): ?>
    <div class="post-image-container">
      <a href="<?php echo $post->url; ?>">
        <img src="<?php echo $post->post_image->url; ?>"
        alt="<?php echo $post->post_image->description; ?>" class="pulse-image"
        width="<?php echo $post->post_image->width; ?>" height="<?php echo $post->post_image->height; ?>">
      </a>
    </div>
  <?php endif; ?>
  <div class="article_summary">
    <div class="article-date">
      <i class="fa fa-bookmark"></i>
      <span class="day"><?php  echo date("d F Y", $post->post_date); ?></span>
    </div>
    <div class="article-tags">
      <?php
      $labels = explode(",", $post->labels_post);
      foreach ($labels as $label){ $label = str_replace(" ", "", $label); ?>
      <a href=<?php echo $pages->get(1)->httpUrl . "search/?q=$label"; ?> rel="tag"><?php echo $label; ?></a>
      <?php } ?>
    </div>
    <div class="clearfix"></div>
    <a href="<?php echo $post->url; ?>" title="<?php echo $post->title; ?>">
      <h1 class="article_title title"><?php echo $post->title; ?></h1>
    </a>
    <div class="article_excerpt">
      <?php
      if(isset($excerpt) && $excerpt) {
        echo excerpt($post->post_content, 1000);
      }
      else{
        echo $post->post_content;
      }
      ?>
    </div>
    <div class="clearfix"></div>
    <div class="article-footer">
      <?php if(isset($excerpt) && $excerpt): ?>
        <a class="read-more-button pure-button" href="<?php echo $post->url; ?>"><?php echo __('Read More') ?></a>
      <?php endif; ?>

      <?php if(isset($bottom_labels) && $bottom_labels): ?>
        <div class="article-tags">
          <?php
          $labels = explode(",", $post->labels_post);
          foreach ($labels as $label){ $label = str_replace(" ", "", $label); ?>
          <a href=<?php echo $pages->get(1)->httpUrl . "search/?q=$label"; ?> rel="tag"><?php echo $label; ?></a>
          <?php } ?>
        </div>
      <?php endif; ?>
      <div class="soc-sharing">
        <ul id="share_articles" class="article_share_items">
          <li>
            <a class="fa fa-facebook" href="#" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false"
            rel="nofollow" target="_blank"></a>
          </li>
          <li>
            <a class="fa fa-twitter" href="#" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false"
            rel="nofollow" target="_blank"></a>
          </li>
          <li>
            <a class="fa fa-google-plus" href="#" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false"
            rel="nofollow" target="_blank"></a>
          </li>
          <li>
            <a class="fa fa-pinterest" href="#" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false"
            rel="nofollow" target="_blank"></a>
          </li>
          <li>
            <a class="fa fa-linkedin" href="#" onclick="window.open(this.href, 'windowName', 'width=550, height=600, left=24, top=24, scrollbars, resizable'); return false"
            rel="nofollow" target="_blank"></a>
          </li>
        </ul>
      </div>
      <?php if (isset($pagination) && $pagination): ?>
        <div class='blog-pager' id='blog-pager'>
          <?php
          $posts = $pages->get("/blog/")->children("sort=post_date");
          $newer_post = $posts->getNext($post);
          $older_post = $posts->getPrev($post);
          ?>
          <?php if(!is_null($newer_post)){ ?>
            <span id='blog-pager-newer-link'>
              <a class='blog-pager-newer-link' href="<?php echo $newer_post->httpUrl; ?>" id='Blog1_blog-pager-newer-link' title='Newer Posts'>&lt; Newer Posts</a>
            </span>
            <?php } ?>
            <?php if(!is_null($older_post)){ ?>
              <span id='blog-pager-older-link'>
                <a class='blog-pager-older-link' href="<?php echo $older_post->httpUrl; ?>" id='Blog1_blog-pager-older-link' title='Older Posts'>Older Posts &gt;</a>
              </span>
              <?php } ?>
        </div>
      <?php endif; ?>
    </div>

    <div class='post-footer'>
      <!-- author -->
      <?php if (isset($author) && $author) echo $post->author->render(); ?>
      <!-- /author -->
    </div>

        <?php if($comments) echo wireRenderFile('partials/comments.php', array('post' => $page)); ?>
      </div>
    </div>
