<?php
  //No direct access
  if ($page->url == $_SERVER["REQUEST_URI"])
    // throw new Wire404Exception();
?>
<?php
  $original_lang = $user->language;
  $user->language = $languages->get("english");
  $posts = top_posts($pages->get("/blog/")->children());
  $user->language = $original_lang;
?>
<div class='widget popular-posts'>
  <div class="widget-title"><h2><?php echo ('Popular Posts') ?></h2></div>
  <div class='widget-content popular-posts'>
    <ul>
      <?php foreach($posts as $url => $visits){ $postPW = $pages->get("$url"); ?>
        <li>
          <div class='item-content'>
            <div class='item-thumbnail'>
              <a href='<?php echo $postPW->url; ?>' target=''>
                <img alt='' border='0' height='72' src="<?php echo $postPW->post_image->size(80, 0)->url; ?>" alt="<?php echo $post->post_image->description; ?>" width='72'/>
              </a>
            </div>
            <div class='item-title'>
              <a href='<?php echo $postPW->url; ?>' class="mostVisitedLink"><?php echo $postPW->title;?>
                <span class="detail-span"><?php echo "[$visits]"; ?></span>
              </a>
            </div>
            <!-- <div class='item-snippet'<?php //echo substr($postPW->contenido_post, 0, 140); ?></div> -->
          </div>
          <div style='clear: both;'></div>
        </li>
      <?php } ?>
      <p style='text-align: center;'>Stats provided by <a href='https://piwik.org' style='font-family: Icomoon; color: #FF3030;'>PIWIK©</a></p>
    </ul>
  </div>
</div>
