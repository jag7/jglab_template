<?php
//No direct access
if ($page->url == $_SERVER["REQUEST_URI"])
throw new Wire404Exception();
?>
<footer>
  <div class='footer-container'>
    <div class='pure-u-1 pure-u-md-1-3'>
      <div class='widget popular-posts'>
        <div class="widget-title"><h2><?php echo __('Random Posts') ?></h2></div>
        <div class='widget-content popular-posts'>
          <ul>
            <?php foreach ($pages->get("/blog/")->children("sort=random, limit=3") as $postPW): ?>
              <li>
                <div class='item-content'>
                  <div class='item-thumbnail'>
                    <a href='<?php echo $postPW->url; ?>' target='_blank'>
                      <img alt='' border='0' height='72' src="<?php echo $postPW->post_image->size(80, 0)->url; ?>" alt="<?php echo $post->post_image->description; ?>" width='72'/>
                    </a>
                  </div>
                  <div class='item-title'>
                    <a href='<?php echo $postPW->url; ?>' class="mostVisitedLink"><?php echo $postPW->title; ?></a>
                  </div>
                </div>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
    <div class='pure-u-1 pure-u-md-1-3'>
      <div class='widget ContactForm' id='ContactForm1'>
        <h2 class='title'><?php echo __('Contact Us (Under construction)') ?></h2>
        <a style='color:#ff3030;' href="mailto:jag@jglab.me">jag@jglab.me</a>
        <div class='contact-form-widget'>
          <div class='form'>
            <form name='contact-form'>
              <p><?php echo __('Name') ?></p>
              <input class='contact-form-name' id='ContactForm1_contact-form-name' name='name' size='30' type='text' value='' />
              <p><?php echo __('Email') ?><span style='font-weight: bolder;'>*</span></p>
              <input class='contact-form-email' id='ContactForm1_contact-form-email' name='email' size='30' type='text' value='' />
              <p><?php echo __('Message') ?><span style='font-weight: bolder;'>*</span></p>
              <p><textarea class='contact-form-email-message' cols='29' id='ContactForm1_contact-form-email-message' name='email-message' rows='5'></textarea></p>
              <input class='contact-form-button contact-form-button-submit' id='ContactForm1_contact-form-submit' type='button' value='<?php echo __('Send') ?>' />
              <div style='text-align: center; max-width: 222px; width: 100%'>
                <p class='contact-form-error-message' id='ContactForm1_contact-form-error-message'></p>
                <p class='contact-form-success-message' id='ContactForm1_contact-form-success-message'></p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /ct-wrapper -->
</div>
<div class='clearfix'></div>
<!-- footer -->
<div class='footer-credits'>
    <!-- footer_blog_social_icons -->
    <div class='footer_social-icons'>
      <ul>
        <li>
          <a class='btn-facebook' href='#' title='Facebook'><i class='fa fa-facebook'></i></a>
        </li>
        <li>
          <a class='btn-twitter' href='#' title='Twitter'><i class='fa fa-twitter'></i></a>
        </li>
        <li>
          <a class='btn-gplus' href='#' title='Google Plus'><i class='fa fa-google-plus'></i></a>
        </li>
        <li>
          <a class='btn-youtube' href='#' title='Youtube'><i class='fa fa-youtube-play'></i></a>
        </li>
        <li>
          <a class='btn-behance' href='#' title='Behance'><i class='fa fa-behance'></i></a>
        </li>
        <li>
          <a class='btn-dribbble' href='#' title='Dribbble'><i class='fa fa-dribbble'></i></a>
        </li>
        <li>
          <a class='btn-pinterest' href='#' title='Pinterest'><i class='fa fa-pinterest'></i></a>
        </li>
      </ul>
    </div>
    <div class='clr'></div>
    <p>Copyright &#169; 2017
      <a href='http://jglab.me'>jglab.me</a>
    </p>
    <p class='attribution'>Powered by
      <a href='http://processwire.com' id='attri_bution'>ProcessWire</a>
    </p>
    <p><?php echo __('Under construction') ?></p>
</div>
</footer>
<!-- load js -->
<?php
echo "<script src='" . $config->urls->templates. 'js/main.min.js' ."' type='text/javascript'></script>";

if(isset($options['scripts'])){
  $scripts = array();
  foreach($options['scripts'] as $script){
    switch ($script) {
      case 'jquery':
        // echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js' type='text/javascript'></script>";
      break;
      case 'recaptcha':
        echo "<script defer src='https://www.google.com/recaptcha/api.js' type='text/javascript'></script>";
      break;
      default:
      // echo "<script src='" . AIOM::JS($script) ."' type='text/javascript'></script>";
      array_push($scripts, $script);
      break;
    }
  }
  if(count($scripts)){
    echo "<script src='" . AIOM::JS($scripts) ."' type='text/javascript'></script>";
  }
}
?>

<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"light-bottom"};
</script>

<script type="text/javascript" defer src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//www.jglab.me/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//www.jglab.me/piwik/piwik.php?idsite=1&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

<script type="text/javascript">
  $( document ).ready(function() {
    $.post( "/", {action: 'currently-reading-widget'}, function( data ) {
      // console.debug( "Data Loaded: " + data );
      $('.widget.currently-reading .widget-content').append(data);
      $('.widget.currently-reading .loading-basic').hide();
      $('.widget.currently-reading .ajax-content-hidden').show('slow');
    });
  });
</script>
