<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
		<link rel="icon" type="image/png" href="http://jglab.me/favicon.ico" />
	</head>
	<body>
	<!--Redirect blog to home/first post-->
		<?php
			if($page->title == "blog"){
				//$session->redirect($page->child->url);
				$session->redirect($pages->get("/home/")->url);
			}
		?>
		<h1><?php echo $page->title; ?></h1>
		<?php if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>"; ?>
	</body>
</html>
