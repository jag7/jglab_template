<!-- header -->
<?php
echo $pages->get('/templates/header/')->render(null,
array('title' => $page->title,
'stylesheets' => array(
  /*$config->urls->templates . */'css/comments.css',
)));
?>
<div class="main-container">
  <section id="main" class="pure-u-1 pure-u-md-3-4">
    <article>
      <?php echo wireRenderFile('partials/article.php',
            array('post' => $page,
                  'author' => true,
                  'comments' => true,
                  'excerpt' => false,
                  'pagination' => true,
                  'bottom_labels' => true,
                  'image' => false
                ));
      ?>
    </article>
  </section>
  <!-- sidebar-wrapper -->
  <aside class='sidebar-wrapper pure-u-1 pure-u-md-1-4'>
      <?php echo wireRenderFile('sidebar.php', array()); ?>
  </aside>
  <!-- /sidebar-wrapper -->
</div>
<!-- footer -->
<?php
echo $pages->get('/templates/footer/')->render(null,
array('scripts' => array(
  'jquery',
  /*'js/comments.js',*/
)));
?>

</html>
