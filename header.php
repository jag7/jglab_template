<?php
  //No direct access
  if ($page->url == $_SERVER["REQUEST_URI"])
    throw new Wire404Exception();
  include($_SERVER['DOCUMENT_ROOT'] . $config->urls->templates . "lib/functions.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- <title><?php echo $options['title'] ?></title> -->
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
		<!-- <meta name="description" content="<?php echo $page->site_description; ?>"> -->
		<meta name=viewport content="width=device-width, initial-scale=1">
    <?php
      // if(isset($options['stylesheets'])): ?>
          <!-- <link type="text/css" rel="stylesheet" href="<?php //echo AIOM::CSS($options['stylesheets']); ?>"></link> -->
      <?php //endif; ?>
      <link href="<?php echo $config->urls->templates . '/css/global.min.css' ?>" media="screen, projection" rel="stylesheet" type="text/css" />

  </head>
  <div class="preloader">
    <div class="preloader-content">
      <div class="pulse-preloader pulse2">
        <img class='pulse' src="<?php echo $config->urls->templates ?>/img/preloader.png" alt="">
      </div>
    </div>
  </div>
  <body>
  <header>
    <nav><!-- menu-wrapper -->
      <div class="menu-wrapper pure-g" id="menu">
        <div class="pure-u-1 hidden">
          <div class="pure-menu menu-toogle-container">
            <a class="custom-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
          </div>
        </div>
        <div class="pure-u-1">
          <div class="pure-menu pure-menu-horizontal custom-can-transform">
            <ul class="pure-menu-list float-left">
              <?php foreach($page->header_menu as $menu_entry){ ?>
                <li class="pure-menu-item"><a class='pure-menu-link' href='<?php echo $menu_entry->link_url; ?>'><?php echo $menu_entry->link_text; ?></a></li>
              <?php } ?>
              <li class='pure-menu-item flag'><i class='fa fa-flag-o' aria-hidden='true'></i></li>
              <?php foreach($languages as $language) {
                  $selected = '';
                  if(!$options['pageStack'][0]->viewable($language)) continue;
                  if($user->language->id == $language->id) $selected = 'selected';
                  $url = $options['pageStack'][0]->localUrl($language);
                  echo "<li class='pure-menu-item lang_selector $selected'><a class='pure-menu-link' href='$url'>$language->title</a></li>";
                }
              ?>
            </ul>
            <div class="float-right">
              <ul class="pure-menu-list">
                <?php foreach($page->social_menu_header as $menu_entry): ?>
      	             <li  class="pure-menu-item social_menu_entry">
                       <a href='<?php echo $menu_entry->link_url; ?>' target='_blank'><i class='fa fa-<?php echo $menu_entry->link_text; ?>'></i></a>
                     </li>
                  <?php endforeach; ?>
                <li class='pure-menu-item nt-search' id='nt-search'>
                  <form action='/search/' id='searchform' method='get'>
                    <div class='nt-search-input-wrap'>
                      <input class='nt-search-input' id='s' name='q' placeholder='Search' type='text' value='' />
                    </div>
                    <input class='nt-search-submit' id='go' type='submit' value='Search' />
                    <span class='nt fa fa-search'></span>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
        </div>
      </div>
    </nav><!-- /menu-wrapper -->
    <div class='top-image-container' style="background: no-repeat scroll center 50% / cover transparent url('<?php echo $config->urls->templates ?>img/background-full-medium.jpg');">
  </header>
