<?php
  if($config->ajax) {
    switch($_POST['action']){
      case 'currently-reading-widget':
        require_once($_SERVER['DOCUMENT_ROOT'] . '/projects/goodreads/book.php');
        ?>
        <div class='ajax-content-hidden'>
          <?php
          $grapi = new grapi("jag7");
          $book = $grapi->getBooks('currently-reading', '1', 'date_updated');
          $progress = $grapi->getProgress();
          if(!isset($book[0])){ //Not reading atm, get last read book
            $book = $grapi->getBooks('read', '1', 'date_read');
            $progress = 100;
          }
          if($book[0]->img_origin == 'local'){
            $book[0]->img = '/projects/goodreads/res/' . $book[0]->img;
          }
          ?>
          <a href="<?php echo $book[0]->link ?>" target="_blank">
            <p class='book-title'><?php echo $book[0]->title ?></p>
            <img src="<?php echo $book[0]->img ?>" class = 'sidebar-book'>
            <p><?php echo $progress ?> %</p>
          </a>
        </div>
        <?php break; ?>
    <?php }
    exit;
  }
?>
<!-- header -->
<?php echo $pages->get('/templates/header/')->render(null, array('title' => $page->title)); ?>
<!-- body -->
<div class="main-container">
  <section id="main" class="pure-u-1 pure-u-md-3-4">

      <!-- posts -->
      <?php
      $posts_per_page = 5;
      $page = isset($_GET['page']) ? $_GET['page'] : 0;
      $start = ($page > 0) ? ($page - 1) * $posts_per_page : 0;
      $posts = $pages->get("/blog/")->children("start=$start, limit=$posts_per_page, sort=-post_date");
      foreach ($posts as $post): ?>
      <article>
        <?php echo wireRenderFile('partials/article.php',
              array('post' => $post,
                    'author' => false,
                    'comments' => false,
                    'excerpt' => true,
                    'pagination' => false,
                    'bottom_labels' => false,
                    'image' => true));
        ?>
      </article>
    <?php endforeach; ?>
  </section>
  <!-- sidebar-wrapper -->
  <aside class='sidebar-wrapper pure-u-1 pure-u-md-1-4'>
      <?php echo wireRenderFile('sidebar.php', array()); ?>
  </aside>
  <!-- /sidebar-wrapper -->
  <div class='clearfix'></div>
  <div class="blog-pager" id="blog-pager">
    <?php
    echo $posts->renderPager();
    ?>
  </div>
  <div class="clearfix"></div>
</div><!-- /main-wrapper -->
<!-- footer -->
<?php
echo $pages->get('/templates/footer/')->render(null,
array('scripts' => array(
  'jquery',
)));
?>
</html>
